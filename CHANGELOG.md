# Changelog

## v1.3.1

* dependencies upgraded

## v1.3.0

* allow multiple values for the tags "musicbrainz-artistid" and "musicbrainz-albumartistid"
* fix "from", "to" & "config" cli argument processing

## v1.2.2

* dependencies upgraded

## v1.2.1

* dependencies upgraded

## v1.2.0

* "copy" encoding format added
* "jobs" cli argument added, that lets you set the number of concurrent transcodes

## v1.1.0

* "flac" encoding format added
* resampling quality set to highest/"10"
